#ifndef STORE_H
#define STORE_H 1

#include <algorithm>

template <typename T>
class Store {
 public:
  Store();
  explicit Store(std::size_t size);
  Store(const Store& rhs);
  Store& operator=(const Store& rhs);
  ~Store();

 private:
  T* m_tab;
  std::size_t m_size;
};

template <typename T>
Store<T>::Store() : m_tab(nullptr), m_size(0) {}

template <typename T>
Store<T>::Store(std::size_t size) : m_tab(new T[size]), m_size(size) {}

template <typename T>
Store<T>::Store(const Store& rhs)
    : m_tab(new T[rhs.m_size]), m_size(rhs.m_size) {
  std::copy_n(rhs.m_tab, m_size, m_tab);
}

template <typename T>
Store<T>& Store<T>::operator=(const Store& rhs) {
  if (this != &rhs) {
    delete[] m_tab;
    m_tab = new T[rhs.m_size];
    m_size = rhs.m_size;
    std::copy_n(rhs.m_tab, m_size, m_tab);
  }
  return *this;
}

template <typename T>
Store<T>::~Store() {
  delete[] m_tab;
}

#endif  // STORE_H
