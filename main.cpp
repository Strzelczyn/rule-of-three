#include <iostream>
#include "Store.h"

int main() {
  Store<char> obj1(3);
  Store<char> obj2 = obj1;
  Store<char> obj3(obj1);
}
